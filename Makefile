all:
	crystal build -s src/Crystalyser.cr

doc:
	crystal docs

test:
	crystal spec

.PHONY: all doc test
