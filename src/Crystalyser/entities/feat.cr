require "./effect"

module Crystalyser

  # Feat is an Effect, named and ordered with an Id.
  # They are described in the table 5-1.
  class Feat < Effect
    @name : String

    getter name
    setter name

    def initialize(@name, before_install, install, after_install, before_remove, remove, after_remove)
      super(before_install, install, after_install, before_remove, remove, after_remove)
    end
  end

end
