require "./entity"

module Crystalyser

  class Effect < Entity
    @before_install : Proc(Character, Character)
    @install        : Proc(Character, Character)
    @after_install  : Proc(Character, Character)
    @before_remove  : Proc(Character, Character)
    @remove         : Proc(Character, Character)
    @after_remove   : Proc(Character, Character)

    setter before_install, install, after_install, before_remove, remove, after_remove

    def initialize(@before_install, @install, @after_install, @before_remove, @remove, @after_remove)
      super()
    end

    def install!(character)
      @before_install.call && @install.call && @after_install.call
    end

    def remove!(character)
      @before_remove.call && @remove.call && @after_remove.call
    end

  end

  # class Effect < Entity
  #   alias Types = Array(String)
  #   alias Value = Int32
  #
  #   @data : Hash(Types, Value) # ex: ["character", "saves", "for"] => +2
  #
  #   def initialize
  #     @data = Hash.new
  #   end
  # end

end
