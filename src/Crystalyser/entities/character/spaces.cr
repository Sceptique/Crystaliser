module Crystalyser

  class Character
    enum SPACES
      T = 1
      S = 2
      M = 4
      L = 8
      H = 16
      C = 32
    end
  end

end
