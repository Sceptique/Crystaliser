module Crystalyser
  class Character

    module Combat
      def bab
        return [ba] if ba <= 5
        ceil = (ba - 1) / 5
        diff = (ba - 1) % 5 + 1
        (0 .. ceil).map { |b| b * 5 + diff }.to_a.reverse
      end
    end

    include Combat

  end
end
