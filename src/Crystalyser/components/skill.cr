require "./component"

module Crystalyser

  class Skill < Component

    @id :           IDS
    @value_base :   Int8
    @value_bonus :  Int8
    @ability :      Character::ABILITIES

    def initialize(@id, @ability, @value_base = 0, @value_bonus = 0)
      super()
    end
  end

end

require "./skill/*"
