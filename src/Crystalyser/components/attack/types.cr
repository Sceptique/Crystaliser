module Crystalyser

  class Attack
    enum TYPES
      BLUDGEONING
      SLASHING
      PIERCING
      ACID
      COLD
      ELECTRICITY
      FIRE
      SONIC
      FORCE
      HOLY
      UNHOLY
      POISON
      BLEEDING
      DISEASE
    end
  end

end
