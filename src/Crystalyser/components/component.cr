module Crystalyser

  class Component
    alias Id = UInt32

    @@component_id  : Id
    @component_id   : Id

    getter component_id

    @@component_id = 0_u32
    def initialize
      @component_id = Component.new_id
    end

    def self.new_id
      @@component_id += 1
    end
  end

end
