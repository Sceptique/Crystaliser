require "./component"
require "../entities/character"
require "../entities/object"

module Crystalyser

  class Inventory < Component

    @owner   : Character
    @objects : Array(Object)

    def initialize(@owner, @objects = Array(Object).new)
      super()
    end
  end

end
