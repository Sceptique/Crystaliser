
describe Crystalyser::Dice do

  it "dice rolling simple" do
    r = Random.new 0
    ((0..10).map do
      Crystalyser::Dice.roll random: r
    end.to_a).should eq([18, 19, 17, 15, 6, 13, 11, 3, 15, 18, 13])
  end

  it "dice rolling modifier" do
    r = Random.new 0
    m = 2
    ((0..10).map do
      Crystalyser::Dice.roll random: r, modifier: 2
    end.to_a).should eq([18, 19, 17, 15, 6, 13, 11, 3, 15, 18, 13].map do |i|
      i + m
    end)
  end

  it "dice rolling 2 dices" do
    r = Random.new 0
    m = 2
    Crystalyser::Dice.roll(random: r, modifier: 2, dice_number: 2).should eq(18 + 19 + 2)
  end

end
