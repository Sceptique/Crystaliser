describe Crystalyser::Feat do

  it "works" do
    p = ->(c : Crystalyser::Character) { c }
    e = Crystalyser::Feat.new "Random", p, p, p, p, p, p
    (e.entity_id).should_not eq(0)
  end

end
