describe Crystalyser::Character do

  it "works" do
    c1 = Crystalyser::Character.new name: "Camille"
    c2 = Crystalyser::Character.new name: "Jiri"
    i1 = c1.entity_id
    (c2.entity_id).should eq(i1 + 1)
    (c1.name).should eq("Camille")
    (c2.name).should eq("Jiri")
  end

  it "test bab" do
    c = Crystalyser::Character.new name: "Camille"
    c.bab.should eq([0])
    c = Crystalyser::Character.new name: "Camille", ba: 1_i8
    c.bab.should eq([1])
    c = Crystalyser::Character.new name: "Camille", ba: 5_i8
    c.bab.should eq([5])
    c = Crystalyser::Character.new name: "Camille", ba: 6_i8
    c.bab.should eq([6, 1])
    c = Crystalyser::Character.new name: "Camille", ba: 7_i8
    c.bab.should eq([7, 2])
    c = Crystalyser::Character.new name: "Camille", ba: 11_i8
    c.bab.should eq([11, 6, 1])
    c = Crystalyser::Character.new name: "Camille", ba: 13_i8
    c.bab.should eq([13, 8, 3])
    c = Crystalyser::Character.new name: "Camille", ba: 15_i8
    c.bab.should eq([15, 10, 5])
    c = Crystalyser::Character.new name: "Camille", ba: 16_i8
    c.bab.should eq([16, 11, 6, 1])
  end

end
